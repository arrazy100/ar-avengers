scene = document.getElementById("scene")
	
const manager = new Hammer.Manager(scene)
const pan = new Hammer.Pan()

manager.add(pan)

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString)

function loadHero(){
	btn = document.getElementById("load_btn")
	parent = btn.parentNode
	parent.removeChild(btn)
	loading = document.getElementById("loading")
	loading.style.visibility = "visible"

	entity = document.createElement("a-entity")
	entity.setAttribute("position", {x: 0, y: 1, z: -2})
	entity.setAttribute("scale", {x: 0.03, y: 0.03, z: 0.03})
	entity.setAttribute("rotation", {x: 0, y: 180, z: 0})
	entity.setAttribute("gltf-model", "https://raw.githubusercontent.com/arrazy100/ar-avengers-assets/master/scene/heroDraco.gltf")
	entity.addEventListener("model-loaded", ()=>{
		loading.style.visibility = "hidden"
	});
	scene.appendChild(entity)
}

camera = document.getElementById("camera")
light = document.getElementById("light")

function go(speed){
	pos = camera.getAttribute("position")
	pos.y += speed
	camera.setAttribute("position", pos)
	light.setAttribute("position", pos)
}

function goFB(speed){
	var angle = camera.getAttribute("rotation")
	var x = speed * Math.cos(angle.y * Math.PI / 180)
	var y = speed * Math.sin(angle.y * Math.PI / 180)
	var pos = camera.getAttribute("position")
	pos.x -= y
	pos.z -= x
	camera.setAttribute("position", pos)
	light.setAttribute("position", pos)
}

manager.on("panleft", function(){
	const rotation = camera.getAttribute("rotation")
	rotation.y = rotation.y - 5
	camera.setAttribute("rotation", rotation)
});
manager.on("panright", function(){
	const rotation = camera.getAttribute("rotation")
	rotation.y = rotation.y + 5
	camera.setAttribute("rotation", rotation)
});